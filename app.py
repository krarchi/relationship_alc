from flask import *
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://root:1234@localhost/alc'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app,db)

class rech_plan(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    plan_range_id = db.relationship('rech_plan_range',backref = 'plan' ,lazy=True)

    def __init__(self,id):
        self.id = id

class rech_plan_range(db.Model):
    id = db.Column(db.Integer,primary_key = True)
    plan_id = db.Column(db.Integer, db.ForeignKey('rech_plan.id'))
    config_set_id = db.relationship('rech_config_set',backref = 'range' ,lazy=True)

    def __init__(self,id):
        self.id=id

class rech_config_set(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    range_id= db.Column(db.Integer,db.ForeignKey('rech_plan_range.id'))
    config_id = db.relationship('rech_config_',backref = 'config' ,lazy=True)

    def __init__(self,id):
        self.id = id

class rech_config_(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    set_id = db.Column(db.Integer,db.ForeignKey('rech_config_set.id'))
    name = db.Column(db.String)
    def __init__(self,id,name):
        self.id = id
        self.name = name


def merge_list(l1,l2):
    for i in l1:
        l2.append(i)
    return l2

@app.route("/")
def main():
    ans = rech_plan.query.filter(rech_plan.id == 3).all()
    for i in ans:
        for j in i.plan_range_id:
            for k in j.config_set_id:
                for l in k.config_id:
                    print l.name
    
    return "Done"
    

    
    """n = 2
    ans = db.session.query(rech_config_).join(rech_config_set,rech_config_.set_id==rech_config_set.id).join(rech_plan_range,rech_plan_range.id==rech_config_set.range_id).join(rech_plan,rech_plan.id==rech_plan_range.plan_id).filter(rech_plan.id==n).all()
    for i in ans:
        print i.name
    return "Done"
    n = 2
    id_1 = rech_plan_range.query.filter_by(plan_id=n).all()
    id_2 = []
    for i in id_1:
        n = rech_config_set.query.filter_by(range_id=i.id)
        id_2=merge_list(n,id_2)
    for i in id_2:
        n = rech_config_.query.filter_by(set_id=i.id)
        for j in n:
            print j.name,j.id
    return "Worked : output is in terminal" """
if __name__=="__main__":
    app.run()
    





